#pragma once

#include <fmt/core.h>
#include <fmt/color.h>
#include <inttypes.h>
#include <string>

namespace TinyLog {
    enum MessageType {
        Title,
        Success,
        Warning,
        Error,
        Regular,
        Emphased
    };

    void debug(bool active, MessageType type, std::string message, char decorator = ' ', size_t max_len = 0);
    void message(bool silent, MessageType type, std::string message, char decorator = ' ', size_t max_len = 0);
    void box(bool silent, MessageType type, std::string message, uint16_t width);
    std::string toHex (const uint8_t *src, size_t len);
};
