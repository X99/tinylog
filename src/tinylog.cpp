#include "tinylog.h"

namespace TinyLog {
    void debug(bool active, MessageType type, std::string message, char decorator, uint16_t max_len) {
        if (active != true)
            return;

        if (decorator == ' ' && max_len != 0)
            max_len = 0;

        if (max_len == 0)
            decorator = ' ';

        fmt::v8::text_style style;
        std::string emoji = "";
        int16_t pad_size = 0;

        switch (type) {
            case Title:
                style = fmt::emphasis::bold | fg(fmt::color::dark_orchid);
                pad_size = max_len - (message.length() + emoji.length() + 1);
                message.insert(0, "\n");
                message += (std::string(" ") + std::string(pad_size > 0 ? pad_size:0, decorator));
                break;

            case Success:
                style = fmt::emphasis::bold | fg(fmt::color::green);
                emoji = "✅ ";
                break;

            case Warning:
                style = fmt::emphasis::bold | fg(fmt::color::orange);
                emoji = "⚠️  ";
                break;

            case Error:
                style = fmt::emphasis::bold | fg(fmt::color::red);
                emoji = "❌ ";
                break;

            case Regular:
            default:
                break;
        }

        fmt::print(style, "{}{}\n", emoji, message);
    }

    void message(MessageType type, std::string message, char decorator, uint16_t max_len) {
        debug(true, type, message, decorator, max_len);
    }

    void box(MessageType type, std::string message, uint16_t width) {
        fmt::v8::text_style style;

        switch (type) {
            case Title:
                style = fmt::emphasis::bold | fg(fmt::color::dark_orchid);
                break;

            case Success:
                style = fmt::emphasis::bold | fg(fmt::color::green);
                break;

            case Warning:
                style = fmt::emphasis::bold | fg(fmt::color::orange);
                break;

            case Error:
                style = fmt::emphasis::bold | fg(fmt::color::red);
                break;

            case Emphased:
                style = fmt::emphasis::bold | fg(fmt::color::white);
                break;

            case Regular:
            default:
                break;
        }

        fmt::print(style,
            "┌{0:─^{2}}┐\n"
            "│{1: ^{2}}│\n"
            "└{0:─^{2}}┘\n", "", message, width - 3);
    }

    std::string toHex (const uint8_t *src, size_t len) {
        std::string out = "";
        for(size_t i = 0; i < len; ++i) {
            if (i > 0 && i%32 == 0)
                out += "\n";
            out += fmt::format("{0:02x}", (uint8_t)src[i]);
        }
        return out;
    }
};
