# Example

```
#include "tinylog.h"

bool dbg = true;
bool silentMode = false;

int main() {
    uint16_t width = 130;

    TinyLog::debug(dbg, TinyLog::Regular, "Regular");
    TinyLog::debug(dbg, TinyLog::Title, "Title", '-', width);
    TinyLog::debug(dbg, TinyLog::Success, "Success");
    TinyLog::debug(dbg, TinyLog::Warning, "Warning");
    TinyLog::debug(dbg, TinyLog::Error, "Error");
    TinyLog::debug(dbg, TinyLog::Error, fmt::format("Numerical value sould be {} or {}", 42, 99.9));

    TinyLog::message(silentMode, TinyLog::Regular, "Regular");

    TinyLog::box(silentMode, TinyLog::Regular, "Regular", width);
    TinyLog::box(silentMode, TinyLog::Title, "Title", width);
    TinyLog::box(silentMode, TinyLog::Success, "Success", width);
    TinyLog::box(silentMode, TinyLog::Warning, "Warning", width);
    TinyLog::box(silentMode, TinyLog::Error, "Error", width);
    TinyLog::box(silentMode, TinyLog::Error, fmt::format("Numerical value sould be {} or {}", 42, 99.9), width);
}
```